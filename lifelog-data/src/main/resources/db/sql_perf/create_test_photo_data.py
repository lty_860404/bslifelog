#!/usr/bin/env python
# -*- encoding : utf8 -*-
from datetime import datetime, time, date, timedelta
users=[('Luora',1)]
users.extend([('John',2)]*3)
users.extend([('Tina',3)]*6)
start_date = datetime.combine(date(2014,05,03),time(12,10))

date = start_date
for i in range(50000) :
	user = users[i % 9]
	sql =[ '''insert into PHOTO ( OWNER_ID, CONTENT_TYPE, LATEST_UPDATE_TIME, UPLOAD_TIME,
					 OWNER_COMMENT, FILEPATHS) 
	''', 
	'	values ( ' , str(user[1]) , ',' ,
	','.join([ ''.join(['\'',str(e),'\'']) 
			for e in ['image/jpeg',date,date,'comment', ''.join(['/s3/p', str(i), '.jpg'])]]) ,
	');']
	print ''.join(sql)
	date = date + timedelta(minutes = 12)
