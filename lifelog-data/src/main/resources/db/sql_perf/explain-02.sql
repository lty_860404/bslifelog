EXPLAIN
-- select distinct p.*
--     from PHOTO p  
--         join FAMILY_MEMBER fm
--             on fm.USER_ID = p.OWNER_ID 
--         left join EVENT_PHOTO_STREAM eps
--             on p.ID = eps.PHOTO_ID
--         left join FAMILY_EVENT fe
--             on fe.EVENT_ID = eps.EVENT_ID
--     where fm.FAMILY_ID = 1 or fe.FAMILY_ID = 1
--     order by p.LATEST_UPDATE_TIME desc
--     limit 10 offset 2000; 
select p.*
    from PHOTO p  
        left join FAMILY_MEMBER fm
             on p.OWNER_ID = fm.USER_ID and fm.FAMILY_ID = 1
        left join EVENT_PHOTO_STREAM eps
            on p.ID = eps.PHOTO_ID
        left join EVENT_ATTENDEE eat 
            on eat.EVENT_ID = eps.EVENT_ID and eat.ATTENDEE_ID = 1 and eat.IS_OWNER = 1 
    where fm.FAMILY_ID = 1 or eat.ATTENDEE_ID = 1 
    order by p.LATEST_UPDATE_TIME desc
    limit 0, 10;

