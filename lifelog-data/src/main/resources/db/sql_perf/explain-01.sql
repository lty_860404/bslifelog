EXPLAIN
select * from PHOTO 
    where OWNER_ID in ( 
        select USER_ID from FAMILY_MEMBER where FAMILY_ID = 1 and STATUS = 1 
        ) or 
        ID in (
            select PHOTO_ID from EVENT_PHOTO_STREAM where EVENT_ID in (
                    select distinct EVENT_ID from EVENT_ATTENDEE
                        where ATTENDEE_ID = 1
                            and IS_OWNER = 1
                ) 
        )
    order by LATEST_UPDATE_TIME desc 
    limit 10 offset 2000;
