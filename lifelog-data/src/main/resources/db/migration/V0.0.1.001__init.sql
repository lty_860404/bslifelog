/*
 * A master table to define relation ship within family,
 * which is used in display the relationship selection page.
 * Then the NAME will be saved to FAMILY_MEMBER table to avoid
 * an additional join.
 */
create table M_RELATIONSHIP (
	ID int not null primary key auto_increment,
	NAME varchar(255) not null,
	LANG_CODE char(10) not null
) engine = innodb auto_increment = 1;

/*
 * An Internationalization master table, which used's create a 
 * family name considering user langage code.
 * The DISPLAY_NAME will be saved to FAMILY NAME column.
 */
create table M_FAMILY_DISPLAYNAME (
	ID int not null primary key auto_increment,
	DISPLAY_NAME varchar(255) not null,
	LANG_CODE char(10) not null
) engine = innodb auto_increment = 1;

/*
 * For difference Event type, provide the I18N attributs' label name.
 * Then DISPLA_NAME will be saved to EVENT_ATTRIBUTE ATTR_NAME column,
 * to avoid an additional join.
 *
 */
create table M_EVENT_ATTR (
	ID int not null primary key auto_increment,
	EVENT_TYPE_CODE int not null,
	ATTR_CODE int not null,
	DISPLAY_NAME varchar(100) not null,
	LANG_CODE char(10) not null,
	index M_EVE_ATR_LAN_COD_ATR_COD_IDX1 ( ATTR_CODE, LANG_CODE)
) engine = innodb auto_increment = 1;

create table FAMILY (
	ID  bigint(20) not null primary key auto_increment,
	NAME varchar(255) not null 
) engine = innodb auto_increment = 1;

create table USER (
	ID bigint(20) not null primary key auto_increment,
	FAMILY_ID bigint(20),
	USER_ID varchar(100) unique key,
	USER_NAME varchar(255),
	PASSWORD varchar(100) not null,
	PHONE_NUMBER varchar(20) unique key,
	AVATOR_PICTURE varchar(255),
	REGISTERED_DATETIME datetime,
	index USER_FAMILY_IDX1 ( FAMILY_ID ),
	foreign key ( FAMILY_ID )
		references FAMILY(ID)
		on delete cascade
) engine = innodb auto_increment = 1;

create table USER_VERIFICATION_CODE (
	ID bigint(20) not null primary key auto_increment,
	USER_ID bigint(20),
	VERIFICATION_CODE char(4),
	index USR_VER_COD_USR_ID_IDX1 (USER_ID),
	foreign key ( USER_ID )
		references USER(ID)
		on delete cascade
) engine = innodb auto_increment = 1;

/*
 * STATUS : 0 - Not Accept yet, 1 - Accepted
 */
create table FAMILY_MEMBER_INVITATION (
	ID bigint(20) not null primary key auto_increment,
	PROMOTER_ID bigint(20),
	PROMOTER_FAMILY_ID bigint(20),
	RELATION_SHIP varchar(255),
	INVITEE_PHONE_NUMBER varchar(20),
	INVITEE_USER_NAME varchar(255),
	STATUS int default 0,
	index FAM_MEM_INV_INV_PHO_NUM_IDX1 (INVITEE_PHONE_NUMBER),
	index FAM_MEM_INV_PRO_ID_IDX1 (PROMOTER_ID),
	foreign key ( PROMOTER_ID )
		references USER(ID)
		on delete cascade
) engine = innodb auto_increment = 1;

/*
 * STATUS : 0 - NOT SIGNIN yet, 1 - signined
 */
create table EXTERNAL_INVITATION (
	ID bigint(20) not null primary key auto_increment,
	PROMOTER_ID bigint(20),
	INVITEE_PHONE_NUMBER varchar(20),	
	STATUS int default 0,
	index FRI_INV_PRO_ID_IDX1 ( PROMOTER_ID ),
	foreign key ( PROMOTER_ID )
		references USER(ID)
		on delete cascade
) engine = innodb auto_increment = 1;

/*
 * STATUS : 1 - is family member, 2 - be blocked
 */
create table FAMILY_MEMBER (
	ID bigint(20) not null primary key auto_increment,
	FAMILY_ID bigint(20),
	USER_ID bigint(20),
	RELATIONSHIP varchar(255),
	STATUS int default 1,
	index FAM_MEM_FAM_ID_IDX1 ( FAMILY_ID ),
	foreign key ( FAMILY_ID )
		references FAMILY(ID)
		on delete cascade,
	index FAM_MEM_USR_ID_IDX1 ( USER_ID ),
	foreign key ( USER_ID)
		references USER(ID)
		on delete cascade

) engine=innodb auto_increment = 1;

-- BELIKED_NUM update: set BELIKED_NUM = BELIKED_NUM + 1
-- BELIKED update : set BELIKED = concat( "USER_NAME@USER_ID,", BELIKED)
-- LATEST_COMMENT : set LATEST_COMMENT = concat ( "USER_NAME@USER_ID:some\,comment\:comment,", LATEST_COMMENT)
create table PHOTO (
	ID bigint(20) not null primary key auto_increment,	
	OWNER_ID bigint(20),
	CONTENT_TYPE varchar(255),
	LATEST_UPDATE_TIME datetime,
	UPLOAD_TIME datetime,
	BELIKED_NUM int default 0,
	BELIKED varchar(2000) default '',
	LATEST_COMMENT varchar(500) default '',
	OWNER_COMMENT text,
	FILEPATHS text not null,
	index PHO_OWN_ID_IDX1 (OWNER_ID),
	foreign key ( OWNER_ID )
		references USER(ID)
		on delete cascade
) engine=innodb auto_increment = 1;

create table PHOTO_LIKED (
	ID bigint(20) not null primary key auto_increment,
	PHOTO_ID bigint(20),		
	BE_LIKED_BY bigint(20), 
	index PHO_LIK_PHO_ID_IDX1 ( PHOTO_ID ),
	foreign key ( PHOTO_ID )
		references PHOTO(ID)
		on delete cascade,
	index PHO_LIK_BE_LIK_BY_IDX1 ( BE_LIKED_BY ),
	foreign key ( BE_LIKED_BY )
		references USER(ID)
		on delete cascade
) engine=innodb auto_increment = 1;

create table PHOTO_COMMENT (
	ID bigint(20) not null primary key auto_increment,
	PHOTO_ID bigint(20),
	COMMENTED_BY bigint(20),
	COMMENT text,
	index PHO_COM_PHO_ID_IDX1 ( PHOTO_ID ),
	foreign key ( PHOTO_ID )
		references PHOTO(ID)
		on delete cascade,
	index PHO_COM_COM_BY_IDX1 ( COMMENTED_BY ),
	foreign key ( COMMENTED_BY )
		references USER(ID)
		on delete cascade
) engine=innodb auto_increment = 1;

create table EVENT (
	ID bigint(20) not null primary key auto_increment,
	EVENT_TYPE_CODE int default 0,
	EVENT_NAME varchar(255) not null,
	BEGIN_DATETIME datetime,
	END_DATETIME datetime
) engine=innodb auto_increment = 1;


create table EVENT_ATTRS (
	ID bigint(20) not null primary key auto_increment,
	EVENT_ID bigint(20),
	ATTR_NAME varchar(100),
	ATTR_VALUE varchar(255),
	index EVE_ATR_EVE_ID_IDX1 ( EVENT_ID ),
	foreign key ( EVENT_ID )
		references EVENT(ID)
		on delete cascade
) engine=innodb auto_increment = 1;

create table EVENT_PHOTO_STREAM (
	ID bigint(20) not null primary key auto_increment,	
	EVENT_ID bigint(20),
	PHOTO_ID bigint(20),
	index EVE_PHO_STR_EVE_ID_IDX1( EVENT_ID ),
	foreign key ( EVENT_ID )
		references EVENT(ID)
		on delete cascade,
	index EVE_PHO_STR_PHO_ID_IDX1 ( PHOTO_ID ),	
	foreign key ( PHOTO_ID )
		references PHOTO(ID)
		on delete cascade
) engine = innodb auto_increment = 1;
/*
 * STATUS : 0 - is not signup yet. 1 - signuped
 */
create table EVENT_ATTENDEE (
	ID bigint(20) not null primary key auto_increment,
	EVENT_ID bigint(20),
	ATTENDEE_ID bigint(20),
	ATTENDEE_NAME varchar(255),
	ATTENDEE_PHONE_NUMBER varchar(20),
	IS_OWNER boolean,
	STATUS int, 
	index EVE_ATE_EVE_ID_IDX1 ( EVENT_ID ),
	foreign key ( EVENT_ID )
		references EVENT(ID)
		on delete cascade,
	index EVE_ATE_USR_ID_IDX1 ( ATTENDEE_ID ),
	foreign key ( ATTENDEE_ID )
		references USER(ID)
		on delete cascade
) engine=innodb auto_increment = 1;
