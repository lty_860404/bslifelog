-- 检验John是否能看到自己被设定为OWNER的Event
select EVE.EVENT_NAME 
	from EVENT EVE, EVENT_ATTENDEE EATE 
	where EATE.EVENT_ID = EVE.ID 
		and EATE.ATTENDEE_ID = 2 
		and EATE.IS_OWNER = 1;