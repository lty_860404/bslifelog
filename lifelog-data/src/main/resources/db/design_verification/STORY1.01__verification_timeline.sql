-- 検証１：　Luora的时间轴上应该有自己的一张照片和John上传的一张照片.
select p.*
    from PHOTO p  
        left join FAMILY_MEMBER fm
             on p.OWNER_ID = fm.USER_ID and fm.FAMILY_ID = 1
        left join EVENT_PHOTO_STREAM eps
            on p.ID = eps.PHOTO_ID
        left join EVENT_ATTENDEE eat 
            on eat.EVENT_ID = eps.EVENT_ID and eat.ATTENDEE_ID = 1 and eat.IS_OWNER = 1
    where fm.FAMILY_ID = 1 or eat.ATTENDEE_ID = 1 
    order by p.LATEST_UPDATE_TIME desc
    limit 0,10 ;
-- 検証 2：　John的时间轴上应该有自己的一张照片和John上传的一张照片.
select p.*
    from PHOTO p  
        left join FAMILY_MEMBER fm
             on p.OWNER_ID = fm.USER_ID and fm.FAMILY_ID = 2
        left join EVENT_PHOTO_STREAM eps
            on p.ID = eps.PHOTO_ID
        left join EVENT_ATTENDEE eat 
            on eat.EVENT_ID = eps.EVENT_ID and eat.ATTENDEE_ID = 2 and eat.IS_OWNER = 1
    where fm.FAMILY_ID = 2 or eat.ATTENDEE_ID = 2 
    order by p.LATEST_UPDATE_TIME desc
    limit 0,10; 
