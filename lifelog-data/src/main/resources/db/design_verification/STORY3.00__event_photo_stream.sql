START TRANSACTION;
-- STORY3 STEP 1
insert into PHOTO ( ID, OWNER_ID, CONTENT_TYPE, LATEST_UPDATE_TIME, UPLOAD_TIME,
				 OWNER_COMMENT, FILEPATHS) 
	values ( 3, 1, 'image/jpeg', '2014-1-3 08:30:00', '2014-1-3 08:30:00', 
				'My Wedding', '/s3/w1.jpg');
insert into EVENT_PHOTO_STREAM ( EVENT_ID, PHOTO_ID )
	values ( 1, 3);
-- STORY3 STEP 2
insert into PHOTO ( ID, OWNER_ID, CONTENT_TYPE, LATEST_UPDATE_TIME, UPLOAD_TIME,
				 OWNER_COMMENT, FILEPATHS) 
	values ( 4, 2, 'image/jpeg', '2014-1-3 08:40:00', '2014-1-3 08:40:00', 
				'So beatiful boom', '/s3/w2.jpg');
insert into EVENT_PHOTO_STREAM ( EVENT_ID, PHOTO_ID )
	values ( 1, 4);
-- STORY3 STEP 3
insert into PHOTO ( ID, OWNER_ID, CONTENT_TYPE, LATEST_UPDATE_TIME, UPLOAD_TIME,
				 OWNER_COMMENT, FILEPATHS) 
	values ( 5, 3, 'image/jpeg', '2014-1-3 08:40:00', '2014-1-3 08:40:00', 
				'Congratuation!!', '/s3/w3.jpg');
insert into EVENT_PHOTO_STREAM ( EVENT_ID, PHOTO_ID )
	values ( 1, 5);
COMMIT;