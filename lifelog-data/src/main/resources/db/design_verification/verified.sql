/*
 * 用户主时间轴取得PHOTO
 */
select p.*
    from PHOTO p  
        left join FAMILY_MEMBER fm
             on p.OWNER_ID = fm.USER_ID and fm.FAMILY_ID = 1
        left join EVENT_PHOTO_STREAM eps
            on p.ID = eps.PHOTO_ID
        left join EVENT_ATTENDEE eat 
            on eat.EVENT_ID = eps.EVENT_ID and eat.ATTENDEE_ID = 1 and eat.IS_OWNER = 1 
    where fm.FAMILY_ID = 1 or eat.ATTENDEE_ID = 1 
    order by p.LATEST_UPDATE_TIME desc
    limit 0, 10;

/*
 * Event的时间轴
 */ 
select * from PHOTO
    where ID in (
            select PHOTO_ID from EVENT_PHOTO_STREAM where EVENT_ID = 1
        );

/*
 * 用户家庭成员一览
 */

 
/*
 * 用户注册
 */ 
-- create a new family , family name will get from M_FAMILY_DISPLAYNAME using user lang code
insert into FAMILY ( ID, NAME ) 
    values (1, 'Luora\'s family');
-- create a new user
insert into USER (ID, FAMILY_ID,USER_ID, USER_NAME, PASSWORD, PHONE_NUMBER,
                 AVATOR_PICTURE, REGISTERED_DATETIME)
    values ( 1, 1, 'Luora1234', 'Luora', 'password', '+819000000000', 
                '/avator/default.png', '2013-12-24 10:00:00');
-- set myself as a family member
insert into FAMILY_MEMBER ( FAMILY_ID, USER_ID, RELATIONSHIP, STATUS )
    values ( 1, 1, 'Me', 1);
-- update if user is invited by another user
update EXTERNAL_INVITATION 
    set STATUS = 1 
    where INVITEE_PHONE_NUMBER = '+819000000001';
-- update if user has benn invited to some events
update EVENT_ATTENDEE
    set ATTENDEE_ID = 1,
        ATTENDEE_NAME = 'Luora',
        STATUS = 1
    where ATTENDEE_PHONE_NUMBER = '+819000000001';
/*
 *  用户发出家族成员邀请 
 */
insert into FAMILY_MEMBER_INVITATION (PROMOTER_ID, PROMOTER_FAMILY_ID,
                 RELATION_SHIP, INVITEE_PHONE_NUMBER, INVITEE_USER_NAME)
    values ( 1, 1,
                 'Husband', 'John', '+819000000001');
-- 如果该用户为没有注册的用户, 通过SMS发送外部邀请
insert into EXTERNAL_INVITATION ( PROMOTER_ID, INVITEE_PHONE_NUMBER) 
    values ( 1, '+819000000001');
/*
 *  用户接受家族成员邀请
 */
-- 设定和邀请人的关系, 加入家庭成员
insert into FAMILY_MEMBER (FAMILY_ID, USER_ID, RELATIONSHIP, STATUS) 
    values ( 2, 1, 'Wife', 1);
-- 根据FAMILY_MEMBER_INVITATION的信息, 把自己加入到邀请人的家庭成员中
insert into FAMILY_MEMBER (FAMILY_ID, USER_ID, RELATIONSHIP, STATUS) 
    values ( 1, 2, 'Husband', 1);
-- 更新家庭成员邀请,设定为已接受状态.
update FAMILY_MEMBER_INVITATION 
    set STATUS = 1 
    where PROMOTER_ID = 1 
        and INVITEE_PHONE_NUMBER = '+819000000001';
/*
 * 用户拒绝家庭成员邀请
 */
-- 更新家庭成员邀请,设定为已拒绝状态.
update FAMILY_MEMBER_INVITATION 
    set STATUS = 2 
    where PROMOTER_ID = 1 
        and INVITEE_PHONE_NUMBER = '+819000000001';

/*
 * comment on a user's photo
 */
-- insert the comment to PHOTO_COMMENT
insert PHOTO_COMMENT (PHOTO_ID, COMMENTED_BY, COMMENT) 
    values ( 1, 1, 'Very Nice!');
-- update PHOTO using 
update PHOTO 
    set LATEST_UPDATE_TIME = '2014-01-01 12:20:00', 
        LATEST_COMMENT= concat('John@John1234:Very Nice!,', LATEST_COMMENT) 
    where ID = 1;

/*
 * like a user's photo
 */
-- insert the like message to PHOTO_LIKED
insert PHOTO_LIKED (PHOTO_ID, BE_LIKED_BY)
    values ( 1, 2);
update PHOTO 
    set BELIKED_NUM=BELIKED_NUM+1, 
        BELIKED = concat('John@John1234,',BELIKED),
        LATEST_UPDATE_TIME = '2014-01-01 12:20:00'
    where ID = 1;