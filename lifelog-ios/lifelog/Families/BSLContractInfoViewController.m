//
//  BSLContractInfoViewController.m
//  Lifelog
//
//  Created by tianyin luo on 14-3-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLContractInfoViewController.h"
#import "BSLFamilyActionCell.h"
#import "BSLPhotoCell.h"
#import "BSLButtonCell.h"

@interface BSLContractInfoViewController ()


@end

@implementation BSLContractInfoViewController

@synthesize member;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return 120;
    }else{
        return 80;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section > 0){
        return @" ";
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if(indexPath.section == 0){
        BSLFamilyCell *familyCell = [tableView dequeueReusableCellWithIdentifier:@"member" forIndexPath:indexPath];
        [self loadMember:familyCell];
        cell=familyCell;
    }else if(indexPath.section == 1){
        BSLButtonCell *btnCell = [tableView dequeueReusableCellWithIdentifier:@"button" forIndexPath:indexPath];
        [btnCell setSeparatorInset:UIEdgeInsetsZero];
        [btnCell setBtnText:@"添     加"];
        [btnCell.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [btnCell.layer setBorderWidth:0.5f];
        cell=btnCell;
    }
    
    return cell;
}



- (void) loadMember:(BSLFamilyCell *)cell{
    if(self.member!=nil && cell!=nil){
        [cell.titleLabel setText:self.member.userName];
        [cell.detailLabel setText:self.member.mobileNum];
        NSLog(self.member.mobileNum);
        if(self.member.photoUrl!=nil){
            [cell setPhoto:[UIImage imageNamed:self.member.photoUrl]];
        }else{
            [cell setPhoto:[UIImage imageNamed:@"Avatar-Init.png"]];
        }
    }
}

- (void) setMemberInfo:(BSLFamilyInfo *)mem{
    [self setMember:mem];
    BSLFamilyCell *familyCell = [self.tableView dequeueReusableCellWithIdentifier:@"member"];
    [self loadMember:familyCell];
    [self.tableView reloadInputViews];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

-(IBAction) navBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
