//
//  BSLContractInfoViewController.h
//  Lifelog
//
//  Created by tianyin luo on 14-3-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSLFamilyInfo.h"

@interface BSLContractInfoViewController : UITableViewController

@property (strong,nonatomic) BSLFamilyInfo *member;

- (void) setMemberInfo:(BSLFamilyInfo *)mem;
-(IBAction) navBack:(id)sender;

@end
