//
//  BSLFamiliesCell.h
//  Lifelog
//
//  Created by tianyin luo on 14-3-19.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSLFamilyCell.h"

@interface BSLFamilyActionCell : BSLFamilyCell

    @property (strong,nonatomic) IBOutlet UIButton *actionBtn;

    -(void) setInviteFlag:(Boolean)flag;

@end
