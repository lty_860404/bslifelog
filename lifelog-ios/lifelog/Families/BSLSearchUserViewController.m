//
//  BSLSearchUserViewController.m
//  Lifelog
//
//  Created by tianyin luo on 14-3-19.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLSearchUserViewController.h"
#import "BSLSearchCell.h"
#import "BSLFamilyInfo.h"
#import "BSLContractListViewController.h"
#import "BSLNavViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "BSLContractInfoViewController.h"

@interface BSLSearchUserViewController ()<ABPeoplePickerNavigationControllerDelegate>
@end

@implementation BSLSearchUserViewController

BSLSearchCell *searchCell;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
//    [self checkAddressBookAccess];
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(section == 0){
        return 2;
    }else{
        return 1;
    }
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section > 0){
        return @" ";
    }
    return nil;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchID = searchBar.text;
    BSLFamilyInfo *searchResultData = [self getPersonByID:searchID];
    if (searchResultData){
        //存在，跳转
        [self pushToContractsInfoViewController:searchResultData];
    }else{
        //不存在弹窗
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"该用户不存在"
                                                       message:@"无法找到该用户，请检查你输入的ID是否正确"
                                                      delegate:nil
                                             cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                             otherButtonTitles:nil];
        [alert show];
    }
}

-(BSLFamilyInfo *)getPersonByID:(NSString *)searchID
{
    if ([@"aida" isEqual:searchID])
    {
        BSLFamilyInfo *aida = [[BSLFamilyInfo alloc] init];
        [aida setPhotoUrl:@"Avatar-Init.png"];
        [aida setUserName:@"艾达"];
        [aida setPersonalSignature:@"我是艾达，这是我的签名"];
        [aida setID:@"aida"];
        [aida setMobileNum:@"+8613888888888"];
        return aida;
    }
    return nil;
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchCell backgroundTap:searchBar];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section == 0 && indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"text" forIndexPath:indexPath];
        [cell.textLabel setText:@"通过用户ID添加"];
    }else if (indexPath.section == 0 && indexPath.row == 1){
        if (searchCell == nil){
            searchCell = [tableView dequeueReusableCellWithIdentifier:@"search" forIndexPath:indexPath];
            
            self.view.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:searchCell action:@selector(backgroundTap:) ];
            singleTap.numberOfTapsRequired = 1;
            singleTap.numberOfTouchesRequired = 1;
            [self.tableView.backgroundView addGestureRecognizer:singleTap];
        }
        cell = searchCell;
    }else if (indexPath.section == 1 && indexPath.row == 0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"contract" forIndexPath:indexPath];
        [cell.textLabel setText:@"添加手机联系人"];
        [cell.detailTextLabel setText:@""];
        [cell.layer setMasksToBounds:YES];
        [cell.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [cell.layer setBorderWidth:0.5f];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(indexPath.section == 1 && indexPath.row ==0){
        [self showPeoplePickerController];
	}
}

- (void) showPeoplePickerController
{
    BSLContractListViewController *contractListView = [[BSLContractListViewController alloc] init];
    contractListView.peoplePickerDelegate = self;
	// Show the picker
    [self.navigationController presentViewController:contractListView animated:YES completion:nil];
}

#pragma mark ABPeoplePickerNavigationControllerDelegate methods
// Displays the information of a selected person
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    BSLFamilyInfo *family = [self getFamilyInfoByRecordRef:person];
    if(family.mobileNum == nil || [@"" isEqualToString:family.mobileNum]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"不支持非移动号码"
                                                       message:@"请确认该用户资料中保存有移动号码"
                                                      delegate:nil
                                             cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                             otherButtonTitles:nil];
        [alert show];
    }else{
        [peoplePicker dismissViewControllerAnimated:NO completion:nil];
        [self pushToContractsInfoViewController:family];
    }
   return NO;
}

-(BSLFamilyInfo *) getFamilyInfoByRecordRef:(ABRecordRef)person
{
    NSString *mobileNum = @"";
    ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
    for (CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++) {
        NSString *mobileLabel = (__bridge NSString *)(ABMultiValueCopyLabelAtIndex(phoneNumbers, i));
        if ([mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel]) {
            mobileNum = (__bridge NSString *)(ABMultiValueCopyValueAtIndex(phoneNumbers,i));
            break;
        }
    }
    
    CFStringRef xing = ABRecordCopyValue(person, kABPersonLastNameProperty);
    CFStringRef ming = ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString *username = [[NSString alloc] initWithFormat:@"%@ %@",(__bridge NSString *)xing,(__bridge NSString*)ming];
    
    BSLFamilyInfo *family = [[BSLFamilyInfo alloc] init];
    [family setMobileNum:mobileNum];
    [family setUserName:username];
    return family;
}

-(void) pushToContractsInfoViewController:(BSLFamilyInfo *)family
{
    UIStoryboard *familyStory = [UIStoryboard storyboardWithName:@"Family" bundle:nil];
    BSLContractInfoViewController *contractInfoView = [familyStory instantiateViewControllerWithIdentifier:@"contractInfoView"];
    [contractInfoView setMemberInfo:family];
    [self.navigationController pushViewController:contractInfoView animated:YES];
}

// Does not allow users to perform default actions such as dialing a phone number, when they select a person property.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
								property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
	return NO;
}

// Dismisses the people picker and shows the application when users tap Cancel.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
	[peoplePicker dismissViewControllerAnimated:YES completion:NULL];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

-(IBAction) navBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



@end
