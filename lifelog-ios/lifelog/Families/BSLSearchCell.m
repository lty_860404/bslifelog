//
//  BSLSearchCell.m
//  Lifelog
//
//  Created by tianyin luo on 14-3-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLSearchCell.h"

@implementation BSLSearchCell

@synthesize searchBar;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

//获取当前元素的controllerview
- (UIViewController *)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

//点击background后键盘取消
-(IBAction)backgroundTap:(id)sender
{
    UIViewController *controller = [self viewController];
    // When the user presses return, take focus away from the text field so that the keyboard is dismissed.
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f, 20.0f, controller.view.frame.size.width, controller.view.frame.size.height);
    controller.view.frame = rect;
    [UIView commitAnimations];
    [self.searchBar resignFirstResponder];
    
}



@end