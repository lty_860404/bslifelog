//
//  BSLFamilyCell.h
//  Lifelog
//
//  Created by tianyin luo on 14-3-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLFamilyCell : UITableViewCell

@property (strong,nonatomic) IBOutlet UILabel *detailLabel;
@property (strong,nonatomic) IBOutlet UILabel *titleLabel;

-(void) setPhoto:(UIImage *)image;

@end
