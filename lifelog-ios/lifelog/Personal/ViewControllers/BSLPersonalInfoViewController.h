//
//  BIUserInfoViewController.h
//  lifelog
//
//  Created by tianyin luo on 14-2-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSLPersonalInfo.h"

@interface BSLPersonalInfoViewController : UITableViewController

    @property (strong,nonatomic) NSArray *keys;
    @property (strong,nonatomic) BSLPersonalInfo *personalInfo;

    -(void) updateDetailForSelectedRow:(NSString *)detailText;
    
@end
