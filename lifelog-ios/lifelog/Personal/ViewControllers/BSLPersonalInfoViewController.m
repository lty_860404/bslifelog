//
//  BIUserInfoViewController.m
//  lifelog
//
//  Created by tianyin luo on 14-2-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLPersonalInfoViewController.h"
#import "BSLEditDetailViewController.h"
#import "BSLPhotoCell.h"
#import "BSLButtonCell.h"

@interface BSLPersonalInfoViewController ()
-(NSDictionary *) refreshCellDic:(UITableView *)tableView;
@end

@implementation BSLPersonalInfoViewController

@synthesize keys;
@synthesize personalInfo;

NSDictionary *cellDic;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.personalInfo = [[BSLPersonalInfo alloc] init];
    [self.personalInfo setPhotoUrl:@"Avatar-Init.png"];
    [self.personalInfo setUserName:@"思远"];
    [self.personalInfo setPersonalSignature:@"lalalalilalilali"];
    [self.personalInfo setID:@"327"];
    [self.personalInfo setMobileNum:@"+8613888888888"];
    
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(cellDic == nil){
        [self refreshCellDic:tableView];
    }
    NSArray *sectionInfo = (NSArray *)[cellDic objectForKey:[NSNumber numberWithInteger:section]];
    return sectionInfo.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section > 0){
        return @" ";
    }
    return nil;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    if(cellDic == nil){
        [self refreshCellDic:tableView];
    }
    return cellDic.count;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0 && indexPath.row == 0){
        return 150;
    }else if(indexPath.section==1 && indexPath.row ==1){
        return 100;
    }else{
        return 50;
    }
}

-(NSDictionary *) refreshCellDic:(UITableView *)tableView{
    
    if(cellDic == nil){
        //TEAM 1
        //photo
        BSLPhotoCell *photoCell = [[BSLPhotoCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier:@"Image"];
        UIImage *photo = [UIImage imageNamed:self.personalInfo.photoUrl];
        [photoCell setPhoto:photo];
        [photoCell.titleLabel setText:@"手机号码"];
        [photoCell.detailLabel setText:@"+86 138 8888 8888"];
        //Name
        UITableViewCell *nameCell = [tableView dequeueReusableCellWithIdentifier:@"Editable"];
        nameCell.textLabel.text = NSLocalizedString(@"Name", nil);
        nameCell.detailTextLabel.text = self.personalInfo.userName;
        //personal Signature key
        UITableViewCell *signKeyCell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        signKeyCell.textLabel.text = NSLocalizedString(@"PersonalitySignature",nil);
        //personal Signature value
        UITableViewCell *signValueCell = [tableView dequeueReusableCellWithIdentifier:@"Editable"];
        signValueCell.detailTextLabel.text = self.personalInfo.personalSignature;
        
        NSArray *personalInfo1 = [NSArray arrayWithObjects:photoCell,nameCell,signKeyCell,signValueCell,nil];
    
        //TEAM 2
        //ID
        UITableViewCell *idCell = [tableView dequeueReusableCellWithIdentifier:@"Editable"];
        idCell.textLabel.text = NSLocalizedString(@"ID",nil);
        idCell.detailTextLabel.text = self.personalInfo.ID;
        //invite btn cell
        BSLButtonCell *inviteCell = [tableView dequeueReusableCellWithIdentifier:@"button"];
        [inviteCell setTopMsg:NSLocalizedString(@"CanSearchByID", nil)];
        [inviteCell setBtnText:NSLocalizedString(@"InviteBtnText", nil)];
        [inviteCell click:@selector(toInviteFriends:) OnTarget:self];
        
        NSArray *personalInfo2 = [NSArray arrayWithObjects:idCell,inviteCell,nil];
        
        cellDic = [NSDictionary dictionaryWithObjectsAndKeys:personalInfo1, [NSNumber numberWithInteger:0],
                                                             personalInfo2, [NSNumber numberWithInteger:1],
                                                             nil];
    }
    return cellDic;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(cellDic == nil){
        [self refreshCellDic:tableView];
    }
    NSArray *sectionInfo = (NSArray *)[cellDic objectForKey:[NSNumber numberWithInteger:(indexPath.section)]];
    UITableViewCell *cell = [sectionInfo objectAtIndex:indexPath.row];
    return cell;
}


-(void) toInviteFriends:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Msg", nil)
                                                   message:NSLocalizedString(@"InvalidFunction",nil)
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         otherButtonTitles:nil];
    [alert show];
}


- (void) updateDetailForSelectedRow:(NSString *)detailText
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.detailTextLabel.text = detailText;
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        NSString* key = cell.textLabel.text;
        NSString* value = cell.detailTextLabel.text;
        [[segue destinationViewController] setDetailName:key andValue: value];
    }
}

@end
