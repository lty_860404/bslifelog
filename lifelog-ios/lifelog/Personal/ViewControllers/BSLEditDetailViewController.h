//
//  BIEditDetailViewController.h
//  lifelog
//
//  Created by tianyin luo on 14-2-20.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLEditDetailViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) id detailName;
@property (strong, nonatomic) id detailText;

@property (weak, nonatomic) IBOutlet UILabel *detailNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *detailTextField;

-(void) setDetailName:(id)newDetailName andValue:(id)newDetailText;
-(IBAction) updateSelectDetail:(id)sender;
-(IBAction) navBack:(id)sender;
-(IBAction) backgroundTap:(id)sender;

@end
