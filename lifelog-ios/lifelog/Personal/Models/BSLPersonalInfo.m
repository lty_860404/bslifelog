//
//  BSLPersonalInfo.m
//  Lifelog
//
//  Created by tianyin luo on 14-3-17.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLPersonalInfo.h"

@implementation BSLPersonalInfo

@synthesize photoUrl;
@synthesize userName;
@synthesize personalSignature;
@synthesize ID;
@synthesize mobileNum;

@end
