//
//  BIPhotoCell.m
//  lifelog
//
//  Created by tianyin luo on 14-3-10.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLPhotoCell.h"

@implementation BSLPhotoCell

@synthesize photoView;
@synthesize titleLabel;
@synthesize detailLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.photoView = [[UIImageView alloc] initWithFrame:CGRectMake(15.0,25.0,100.0,100.0)];
        
        self.photoView.layer.masksToBounds=YES;
        self.photoView.layer.cornerRadius=self.photoView.frame.size.width/2;
//        self.imageView.layer.borderWidth=1.0;
//        self.imageView.layer.borderColor=[[UIColor yellowColor] CGColor];
        
        self.photoView.userInteractionEnabled = YES;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(popUpToSelectImage:) ];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        [self.photoView addGestureRecognizer:singleTap];
        [self.contentView addSubview:self.photoView];
        
        self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(120, 50, 220, 20)];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:self.titleLabel];
        
        self.detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(140, 80, 220, 20)];
        [self.detailLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:self.detailLabel];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


-(IBAction)popUpToSelectImage:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"PhotoAlertTitle", nil)
                                                   message:NSLocalizedString(@"PhotoAlertMsg",nil)
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         otherButtonTitles:NSLocalizedString(@"SelectFromLocal", nil), NSLocalizedString(@"SelectFromCamera", nil),nil];
    alert.delegate = self;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1){
        [self takeNewPhoto:NO];
    }else if(buttonIndex == 2){
        [self takeNewPhoto:YES];
    }
}

- (UIViewController *)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

//等比例缩放
-(UIImage *)getImage:(UIImage *)image
           withWidth:(CGFloat)width
           andHeight:(CGFloat)height
{
    UIImage * resultImage = image;
    if (image.size.width != width && image.size.height != height)
    {
        CGSize itemSize = CGSizeMake(width,height);
        UIGraphicsBeginImageContext(itemSize);
        CGRect imageRect = CGRectMake(0, 0, width, height);
        [image drawInRect:imageRect];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return resultImage;
}

//处理圆角

//设置照片
- (void) setPhoto:(UIImage *)image{
    UIImage* resultImage = [self getImage:image
                              withWidth:100
                              andHeight:100];
    [self.photoView setImage:resultImage];
}

- (IBAction)takeNewPhoto:(BOOL)isCamera{
    //创建图片选择器
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    if (!isCamera)
    {
        //指定源的类型
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }else{
        //指定源类型前，检查图片源是否可用
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }else{
            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:@"InvalidFunction" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
        }
    }
    //在选定图片之前，用户可以简单编辑要选的图片。包括上下移动改变图片的选取范围，用手捏合动作改变图片的大小等。
    imagePicker.allowsEditing = YES;
    //实现委托，委托必须实现UIImagePickerControllerDelegate协议，来对用户在图片选取器中的动作
    imagePicker.delegate = self;
    //设置完iamgePicker后，就可以启动了。用以下方法将图像选取器的视图“推”出来
    UIViewController *controller = [self viewController];
    if(controller){
        [controller presentViewController:imagePicker animated:true completion:nil];
    }
}

#pragma mark imagePickerDelegate
//用户点击图像选取器中的“cancel”按钮时被调用，这说明用户想要中止选取图像的操作
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//用户点击选取器中的“choose”按钮时被调用，告知委托对象，选取操作已经完成，同时将返回选取图片的实例
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    
    [self setPhoto:image];
    [picker dismissModalViewControllerAnimated:YES];
}
@end