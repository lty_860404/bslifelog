//
//  BIPhotoCell.h
//  lifelog
//
//  Created by tianyin luo on 14-3-10.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLPhotoCell : UITableViewCell<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>

    @property (strong, nonatomic) IBOutlet UIImageView *photoView;
    @property (strong, nonatomic) IBOutlet UILabel *titleLabel;
    @property (strong, nonatomic) IBOutlet UILabel *detailLabel;

    -(void)setPhoto:(UIImage *)image;

@end
