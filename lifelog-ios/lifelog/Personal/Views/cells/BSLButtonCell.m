//
//  BIButtonCell.m
//  lifelog
//
//  Created by tianyin luo on 14-3-13.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLButtonCell.h"

@implementation BSLButtonCell

@synthesize titleLabel;
@synthesize button;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.titleLabel.numberOfLines = 0;
//        self.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
//        self.titleLabel.textAlignment = NSTextAlignmentCenter;
//        [self.contentView addSubview:self.titleLabel];
//        
//        self.button.selected = NO;
//        [self.button.layer setMasksToBounds:YES];
//        [self.button.layer setCornerRadius:5.0f];
//        [self.button.layer setBorderColor:[UIColor lightGrayColor].CGColor ];
//        [self.button.layer setBorderWidth:0.5f];
//        self.accessoryView = self.button;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
//        self.titleLabel.numberOfLines = 0;
//        self.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
//        self.titleLabel.textAlignment = NSTextAlignmentCenter;
//        [self.contentView addSubview:self.titleLabel];
//        
//        self.button.selected = NO;
//        [self.button.layer setMasksToBounds:YES];
//        [self.button.layer setCornerRadius:5.0f];
//        [self.button.layer setBorderColor:[UIColor lightGrayColor].CGColor ];
//        [self.button.layer setBorderWidth:0.5f];
//        self.accessoryView = self.button;
    }
    return self;
}

-(void) setTopMsg:(NSString *)topMsg
{
    [self.titleLabel setText:topMsg];
}

-(void) setBtnText:(NSString *)btnText
{
    [self.button setTitle:btnText forState:UIControlStateNormal];
}

-(void) click:(SEL)action OnTarget:(id)controller{
    [self.button addTarget:controller action:action forControlEvents:UIControlEventTouchDown];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
