//
//  BIPhotoCell.h
//  lifelog
//
//  Created by tianyin luo on 14-3-10.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLPhotoCell : UITableViewCell<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>

    -(IBAction)popUpToSelectImage:(id)sender;

@end
