//
//  BIButtonCell.m
//  lifelog
//
//  Created by tianyin luo on 14-3-13.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLButtonCell.h"

@implementation BSLButtonCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat labelHeight=30;
        CGFloat frameWidth = self.frame.size.width;
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frameWidth, labelHeight)];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.titleLabel];
        
        CGFloat buttonWidth=300;
        CGFloat buttonHeight=45;
        self.button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [self.button setFrame:CGRectMake((frameWidth-buttonWidth)/2,labelHeight,buttonWidth,buttonHeight)];
//        self.button = [[UIButton alloc] initWithFrame:CGRectMake(0,0,buttonWidth,buttonHeight)];
        self.button.selected = NO;
        self.accessoryView = self.button;
    }
    return self;
}

-(void) setTopMsg:(NSString *)topMsg
{
    [self.titleLabel setText:topMsg];
}

-(void) setBtnText:(NSString *)btnText
{
    [self.button setTitle:btnText forState:UIControlStateNormal];
}

-(void) click:(SEL)action OnTarget:(id)controller{
    [self.button addTarget:controller action:action forControlEvents:UIControlEventTouchDown];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
