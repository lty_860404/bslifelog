//
//  BIPhotoCell.m
//  lifelog
//
//  Created by tianyin luo on 14-3-10.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLPhotoCell.h"

@implementation BSLPhotoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.imageView setFrame:CGRectMake(5.0,5.0,120.0,120.0)];
        self.userInteractionEnabled = YES;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(popUpToSelectImage:) ];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        [self addGestureRecognizer:singleTap];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


-(IBAction)popUpToSelectImage:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"PhotoAlertTitle", nil)
                                                   message:NSLocalizedString(@"PhotoAlertMsg",nil)
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         otherButtonTitles:NSLocalizedString(@"SelectFromLocal", nil), NSLocalizedString(@"SelectFromCamera", nil),nil];
    alert.delegate = self;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1){
        [self takeNewPhoto:NO];
    }else if(buttonIndex == 2){
        [self takeNewPhoto:YES];
    }
}

- (UIViewController *)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

- (IBAction)takeNewPhoto:(BOOL)isCamera{
    //创建图片选择器
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    if (!isCamera)
    {
        //指定源的类型
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }else{
        //指定源类型前，检查图片源是否可用
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }else{
            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:@"InvalidFunction" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
        }
    }
    //在选定图片之前，用户可以简单编辑要选的图片。包括上下移动改变图片的选取范围，用手捏合动作改变图片的大小等。
    imagePicker.allowsEditing = YES;
    //实现委托，委托必须实现UIImagePickerControllerDelegate协议，来对用户在图片选取器中的动作
    imagePicker.delegate = self;
    //设置完iamgePicker后，就可以启动了。用以下方法将图像选取器的视图“推”出来
    UIViewController *controller = [self viewController];
    if(controller){
        [controller presentViewController:imagePicker animated:true completion:nil];
    }
}

//用户点击图像选取器中的“cancel”按钮时被调用，这说明用户想要中止选取图像的操作
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//用户点击选取器中的“choose”按钮时被调用，告知委托对象，选取操作已经完成，同时将返回选取图片的实例
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    [self.imageView setImage:image];
    [picker dismissModalViewControllerAnimated:YES];
}
@end
