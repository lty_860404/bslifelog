//
//  BIButtonCell.h
//  lifelog
//
//  Created by tianyin luo on 14-3-13.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLButtonCell : UITableViewCell

    @property (strong,nonatomic) UILabel *titleLabel;
    @property (strong,nonatomic) UIButton *button;

    -(void) setTopMsg:(NSString *)topMsg;
    -(void) setBtnText:(NSString *)btnText;
    -(void) click:(SEL)action OnTarget:(id)controller;

@end
