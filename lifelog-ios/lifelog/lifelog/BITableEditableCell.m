//
//  BITableEditableCell.m
//  lifelog
//
//  Created by tianyin luo on 14-2-24.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BITableEditableCell.h"

@implementation BITableEditableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setValueItem:(id)valueItem
{
    if (self.valueItem != valueItem) {
        self.valueItem = valueItem;
        [self configureCell];
    }
}

- (void)configureCell
{
    if(self.valueItem){
        self.valueLabel.text = self.valueItem;
    }
}


@end
