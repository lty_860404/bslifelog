//
//  BITableSwitchCell.m
//  lifelog
//
//  Created by tianyin luo on 14-2-26.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BITableSwitchCell.h"

@implementation BITableSwitchCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self.switchBtn setOn:YES];
        [self.switchBtn addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)switchAction:(id)sender
{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        NSLog(@"是");
    }else {
        NSLog(@"否");
    }
}


@end
